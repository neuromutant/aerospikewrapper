const Aerospike = require('aerospike');

class AS_CONNECTOR {
    constructor(host = "127.0.0.1", port = 3005) {
        this.client = Aerospike.client({
            hosts: [{ addr: host, port: port }],
            log: {level: Aerospike.log.INFO},
        })

        this.client.connect(function(error) {
            if (error) {
                console.log('\x1b[41m', 'Connection to Aerospike cluster failed!' ,'\x1b[0m');
            } else {
                console.log('\x1b[42m', 'Connection to Aerospike cluster succeeded!' ,'\x1b[0m');
            }
        })
    }
}

let connector = new AS_CONNECTOR();

export {connector as default};

